//SOAL NOMOR 5
console.log('\nSOAL NOMOR 5');
function getSplitName(personName){
    //mengecek apakah nama bukan string
    if(typeof personName !== 'string'){
        return 'ERROR : Bukan String'
    }
    //jika string
    else if(typeof personName === 'string'){
        const name = personName.split(" ")
        const pName = {}
        if(name.length == 3){
            pName.firstName = name[0]
            pName.middleName = name[1]
            pName.lastName = name[2]
        }
        else if(name.length == 2){
            pName.firstName = name[0]
            pName.middleName = null
            pName.lastName = name[1]
        }
        else if(name.length == 1){
            pName.firstName = name[0]
            pName.middleName = null
            pName.lastName = null
        }
        //jika lebih dari 4 characters
        else if(name.length >=4){
            return 'Error : this function is only for 3 characters name'
        }
        else{
            return 'Error'
        }   
        return pName
    }
}
console.log(getSplitName('Aldi Daniela Pranata'))
console.log(getSplitName('Dwi Kuncoro'))
console.log(getSplitName('Aurora'))
console.log(getSplitName('Aurora Aureliya Sukma Darma'))
console.log(getSplitName(0))