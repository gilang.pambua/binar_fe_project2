//SOAL NOMOR 3
console.log('\nSOAL NOMOR 3')
function checkEmail(email) {
    const syaratEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    //cek email kosong atau tidak
    if (typeof email === 'undefined') {
        return 'Email Kosong'
    }
    //cel nilai string
    if (typeof email !== 'string') {
        return 'Email bukan string'
    }
    //cek syarat email
    if (!syaratEmail.test(email)) {
        return 'INVALID'
    }
    return 'VALID'
}
console.log(checkEmail('apranata@binar.co.id'))
console.log(checkEmail('apranata@binar.com'))
console.log(checkEmail('apranata@binar'))
console.log(checkEmail('apranata'))
console.log(checkEmail(3322))
console.log(checkEmail())