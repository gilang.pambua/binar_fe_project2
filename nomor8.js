//SOAL NOMOR 8
console.log('\nSOAL NOMOR 8');
const dataPenjualanNovel = [
    {
        idProduct : 'BOOK002421',
        namaProduct : 'Pulang - Pergi',
        penulis : 'Tere Liye',
        hargaBeli : 60000,
        hargaJual : 86000,
        totalTerjual : 150,
        sisaStok : 17,
    },
    {
        idProduct : 'BOOK002351',
        namaProduct : 'Selamat Tinggal',
        penulis : 'Tere Liye',
        hargaBeli : 75000,
        hargaJual : 103000,
        totalTerjual : 171,
        sisaStok : 20,
    },
    {
        idProduct : 'BOOK002941',
        namaProduct : 'Garis Waktu',
        penulis : 'Fiersa Besari',
        hargaBeli : 67000,
        hargaJual : 99000,
        totalTerjual : 213,
        sisaStok : 56,
    },
    {
        idProduct : 'BOOK002941',
        namaProduct : 'Laskar Pelangi',
        penulis : 'Andrea Hirata',
        hargaBeli : 55000,
        hargaJual : 68000,
        totalTerjual : 20,
        sisaStok : 56,
    },
]
function getInfoPenjualan(dataPenjualan){
    //deklarasi
    let totalKeuntungan = 0
    let totalModal = 0
    let produkBukuTerlaris
    let penulisTerlaris = {}
    let maxterjual = 0
    let persentaseKeuntungan = 0
    let topPenulis= ''

    //loop untuk menghitung untung, modal dan penulis terlaris
    for(let i=0; i<dataPenjualan.length; i++){
        //dataPenjualan disimpan ke product untuk memudahkan penulisan
        let product = dataPenjualan[i] 
        //data untung disimpan kr variabel untung
        let untung = (product.hargaJual - product.hargaBeli) * product.totalTerjual
        //hasil perhitungan untung disimpan ke total keuntungan
        totalKeuntungan += untung;
        //perhitungan total modal
        totalModal += product.hargaBeli * (product.totalTerjual + product.sisaStok)
        //kondisi untuk menentukan produk terlaris dengan variabel maxterjual
        if(product.totalTerjual > maxterjual){
            maxterjual = product.totalTerjual
            produkBukuTerlaris = product
        }
        //penulis terlaris ditentukan dari total produknya yang terjual
        penulisTerlaris[product.penulis] += product.totalTerjual
    }
    // loop untuk menentukan siapa top penulis dengan mengambil value penulis di penulisterlaris
    for(let penulis in penulisTerlaris){
        //menyatakan jika bukan top penulis maka akan menjadikan penulis tersebut sebagai top penulis
        //dimana nilai topPnulis awalnya dideklarasikan ' ' /kosong maka akan dibuat penulisnya menjadi topPenulis
        if(!topPenulis){
            topPenulis = penulis
        }
    }
    //perhitungan untuk persentase keuntungan
    if(totalModal > 0){
        persentaseKeuntungan = (totalKeuntungan / totalModal) * 100
    }

    console.log("Total Keuntungan: Rp. " + totalKeuntungan.toLocaleString(['ban', 'id']))
    console.log("Total Modal: Rp. " + totalModal.toLocaleString(['ban', 'id']))
    console.log("Buku Terlaris: " + produkBukuTerlaris.namaProduct)
    console.log("Penulis Buku Terlaris: " + topPenulis)
    console.log("Persentase Keuntungan: " + persentaseKeuntungan.toFixed(2) + "%")
}
getInfoPenjualan(dataPenjualanNovel);