//SOAL NOMOR 4
console.log('\nSOAL NOMOR 4');
function isValidPassword(password){
    //mengecek apakah password undefined
    if(typeof password === 'undefined'){
        console.log('ERROR : password kosong')
        return false
    }
    //mengecek apakah password string
    if(typeof password !== 'string'){
        console.log('ERROR : password harus string')
        return false
    }
    const syaratPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[\w\W]{8,}$/
    //mengecek apakah password tidak sesuai dengan regular expression
    if(!syaratPassword.test(password)){
        return false
    }
    return true
}
console.log(isValidPassword('Meong2021'))
console.log(isValidPassword('meong2021'))
console.log(isValidPassword('@eong'))
console.log(isValidPassword('Meong2'))
console.log(isValidPassword(0))
console.log(isValidPassword())