//SOAL NOMOR 6
console.log('\nSOAL NOMOR 6');
function getAngkaTerbesarKedua(dataAngka){
    //jika data null
    if(dataAngka == null){
        return 'Error : Data Kosong'
    }
    //jika data bernilai 0
    else if(dataAngka == 0){
        return 'Error : Salah Parameter'
    }
    //sorting angka dan mengambil nilai terbesar kedua
    else{
        return dataAngka.sort()[dataAngka.length-2]
    }
}
const dataAngka = [9,4,7,7,4,3,2,2,8]

console.log(getAngkaTerbesarKedua(dataAngka))
console.log(getAngkaTerbesarKedua(0))
console.log(getAngkaTerbesarKedua())